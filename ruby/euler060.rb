require_relative 'euler'

def set_of_concatenatable_primes_old?(list)
  list.combination(2).map { |q, r| (q.to_s + r.to_s).to_i.prime? and (r.to_s + q.to_s).to_i.prime? }.all?
end

def solution
  Prime.take(200).combination(3).find { |set| set_of_concatenatable_primes?(set) }
end

def set_of_concatenatable_primes?(list)
  list.combination(2).each do |q, r|
    return false unless (q.to_s + r.to_s).to_i.prime? and (r.to_s + q.to_s).to_i.prime?
  end
  true
end
