require 'prime'

#1. generate quadratic expressions
#2. for each quadratic expression, iterate until non-prime is found
#3.
def primes_from_quadratic(a,b)
  generator = Enumerator.new do |y|
    n = 0
    loop do
      y << n**2 + a*n + b
      n += 1
    end
  end

  primes = []

  while (next_num = generator.next).prime?
    primes << next_num
  end
  primes
end

max_consecutive_prime_length = 0
max_consecutive_prime_coefficients = []
(-999..999).each do |a|
  (-999..999).each do |b|
    prime_length = primes_from_quadratic(a,b).count
    if prime_length > max_consecutive_prime_length
      max_consecutive_prime_coefficients = [a,b]
      max_consecutive_prime_length = prime_length
    end
  end
end

puts max_consecutive_prime_coefficients
