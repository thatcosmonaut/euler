def triangular
  triangular_enum = Enumerator.new do |y|
    n = 1
    loop do
      x = n * (n+1) / 2
      n += 1
      y << x
    end
  end
  triangular_enum
end

def pentagonal?(n)
  ((Math.sqrt((24 * n) + 1) + 1) / 6) % 1 == 0
end

def hexagonal?(n)
  ((Math.sqrt((8 * n) + 1) + 1) / 4) % 1 == 0
end

def triangular_pentagonal_and_hexagonal_up_to(n)
  triangular.take_while { |x| x < n }.select { |x| pentagonal?(x) && hexagonal?(x) }
end
