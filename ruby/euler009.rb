require_relative 'euler'

def solution
  Euler.generate_pythagorean_triples(500).find { |x| x.inject(:+) == 1000 }.inject(:*)
end
