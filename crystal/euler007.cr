require "./prime"

module Euler
  module Problem007
    extend self

    def solution
      Euler::Prime.new.skip(10000).next
    end
  end
end
