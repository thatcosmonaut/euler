require_relative 'euler'

def factorization_of_integer_divisible_by_all_integers_up_to(n)
  {}.tap do |factorization|
    (2..n).map do |i|
      Prime.prime_division(i).map do |prime, exponent|
        factorization[prime] = exponent if (!factorization.include?(prime) || (exponent > factorization[prime]))
      end
    end
  end
end

def factors_to_int(factorization)
  factorization.map { |prime, exponent| prime ** exponent }.inject(:*)
end

def solution
  factors_to_int(factorization_of_integer_divisible_by_all_integers_up_to(20))
end
