use itertools::Itertools;

fn palindrome(n: u64) -> bool {
    let num_string = n.to_string();
    let half = num_string.len() / 2;

    num_string.bytes().take(half).eq(num_string.bytes().rev().take(half))
}

pub fn solution() -> Option<u64> {
    (100..1000).tuple_combinations().map( |(a, b)| a * b ).filter( |n| palindrome(*n)).max()
}

#[cfg(test)]
mod tests {
    use euler::problem_004::solution;

    #[test]
    fn problem_004() {
        assert_eq!(solution(), Some(906_609));
    }
}