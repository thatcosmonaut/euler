require "./euler"

module Euler
  module Problem009
    extend self

    def generate_pythagorean_triples(upper_bound)
      ([] of Array(Euler::NumType)).tap do |triples|
        (2..upper_bound).each do |a|
          (a..upper_bound).each do |b|
            c = Math.sqrt(a**2 + b**2)
            triples << [a, b, c.to_i] if c % 1 == 0
          end
        end
      end
    end

    def solution
      generate_pythagorean_triples(500).find([-1]) { |x| x.sum == 1000 }.product
    end
  end
end
