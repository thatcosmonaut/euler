import euler

fn even_fibonacci_up_to(n int) []int {
  mut result := []int
  mut a := 0
  mut b := 1
  for a + b < n {
    if euler.even(a + b) { result << a + b }
    temp := b
    b = a + b
    a = temp
  }
  return result
}

fn main() {
  println(even_fibonacci_up_to(4000000).reduce(euler.add, 0))
}
