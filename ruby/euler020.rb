def factorial(n)
  (1..n).inject(:*)
end

puts factorial(100).to_s.split("").map(&:to_i).inject(:+)
