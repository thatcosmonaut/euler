require_relative 'euler'

def solution
  (2**1000).to_digit_list.inject(:+)
end
