require "spec"
require "../euler001"

describe Euler::Problem001 do
  it "should return 233168" do
    Euler::Problem001.solution.should eq 233168
  end
end
