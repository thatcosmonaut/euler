require "./euler"

module Euler
  module Problem005
    extend self

    def integer_factorization_divisible_by_all_up_to(n)
      result = {} of Euler::NumType | BigInt => Int32
      (2..n).map do |i|
        Euler::Prime.prime_factorization(i).each do |prime, exponent|
          if !result.has_key?(prime) || (exponent > result[prime])
            result[prime] = exponent
          end
        end
      end
      result
    end

    def factors_to_int(factorization : Hash(Euler::NumType | BigInt, Int32))
      factorization.map { |prime, exponent| prime ** exponent }.product
    end

    def solution
      factors_to_int(integer_factorization_divisible_by_all_up_to(20))
    end
  end
end
