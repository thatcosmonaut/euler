def is_pandigital?(num_as_string)
  num_as_string.split("").sort.join == "123456789"
end

pandigitals = []
(1..100000).each do |i|
  (1..100000).each do |j|
    k = i * j
    if is_pandigital?(i.to_s + j.to_s + k.to_s)
      [i,j,k] << pandigitals
      puts [i,j,k].inspect
    end
    break if k.to_s.length > 9
  end
end

puts pandigitals.inspect
