(load "euler.lisp")

(defun fibonacci-list-helper (n builder)
  (if (>= (+ (first builder) (first (rest builder))) n) builder (fibonacci-list-helper n (cons (+ (first builder) (first (rest builder))) builder)))
)

(defun fibonacci-list (n)
  (fibonacci-list-helper n (list 2 1))
)

(defun solution () (sum (select (lambda (a) (eq (mod a 2) 0)) (fibonacci-list 4000000))))
