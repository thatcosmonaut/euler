require "spec"
require "../euler004"

describe Euler::Problem004 do
  it "should return 906609" do
    Euler::Problem004.solution.should eq 906609
  end
end
