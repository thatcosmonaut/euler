(load "euler.lisp")

(defun sum-of-squares-up-to (n)
  (sum (mapcar (lambda (n) (* n n)) (range 1 n)))
)

(defun square-of-sum-up-to (n)
  (let* ((square (sum (range 1 n))))
        (* square square))
)

(defun solution ()
  (- (square-of-sum-up-to 100) (sum-of-squares-up-to 100))
)
