require 'prime'

def four_digit_increasing_by(n)
  return [] if (9999 - (2*n)) < 0
  solutions = []
  (1..(9999 - (2*n))).each do |x|
    solutions << [x, x + n, x + (2 * n)]
  end
  solutions
end

def to_digit_list(num)
  num.to_s.split('').map(&:to_i)
end

def permutations?(number_list)
  comparison_digit_list = to_digit_list(number_list.first).sort
  number_list.all? { |x| to_digit_list(x).sort == comparison_digit_list }
end

def find_prime_permutations(n)
  four_digit_increasing_by(n).select { |x| permutations?(x) }.select { |x| x.all? { |y| Prime.prime?(y) } }
end
