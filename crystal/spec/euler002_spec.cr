require "spec"
require "../euler002"

describe Euler::Problem002 do
  it "should return 4613732" do
    Euler::Problem002.solution.should eq 4613732
  end
end
