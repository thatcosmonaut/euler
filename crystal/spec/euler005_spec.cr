require "spec"
require "../euler005"

describe Euler::Problem005 do
  it "should return 232792560" do
    Euler::Problem005.solution.should eq 232792560
  end
end
