names = []
File.open('names.txt', 'r') do |file|
  names = file.gets.gsub(/"/, "").split(",")
end
names.sort!

def get_alphabet_values
  {}.tap do |alphabet_values|
    i = 1
    ("A".."Z").each do |char|
      alphabet_values[char] = i
      i += 1
    end
  end
end

def alphabet_score(name, alphabet_values)
  score = 0
  name.split("").each { |letter| score += alphabet_values[letter]}
  score
end

def total_score(names, alphabet_values)
  [].tap do |scores|
    names.each_with_index do |name, index|
      scores << alphabet_score(name, alphabet_values) * (index+1)
    end
  end.inject(:+)
end

puts total_score(names, get_alphabet_values)
