require_relative 'euler'

def solution
  Prime.take_while { |x| x < 2000000 }.inject(:+)
end
