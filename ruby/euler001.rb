def multiples_of_three_and_five_below(n)
  (3..n-1).select { |i| (i % 3 == 0) || (i % 5 == 0) }
end

def solution
  multiples_of_three_and_five_below(1000).inject(:+)
end
