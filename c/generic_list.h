#ifndef GENERIC_LIST_H_
#define GENERIC_LIST_H_

typedef enum {
  INT,
  LONG,
  LONG_LONG
} GENERIC_LIST_TYPE;

typedef struct generic_node {
  void* data;
  GENERIC_LIST_TYPE type;
  struct generic_node* next;
} generic_node;

typedef struct generic_list_ {
  int size;
  generic_node *head;
  generic_node *tail;
} generic_list;

void generic_node_init(generic_node *n, GENERIC_LIST_TYPE type, void* value);
void generic_list_init(generic_list *l);
int generic_list_count(generic_list *l);
void generic_list_add(generic_list *l, GENERIC_LIST_TYPE type, void* value);
generic_node generic_list_get(generic_list *l, int index);
void generic_list_set(generic_list *l, int index, void* value);
void generic_list_delete(generic_list *l, int index);
long long generic_list_sum(generic_list *l);
void generic_list_print(generic_list *l);
void generic_list_free(generic_list *l);

#endif
