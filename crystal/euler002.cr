module Euler
  module Problem002
    extend self

    def fibonacci_nums_up_to(n)
      result = [1] of Int32

      a = 1
      b = 2
      accum = 0

      while accum < n
        accum = a + b
        result << b
        a = b
        b = accum
      end

      result
    end

    def solution
      fibonacci_nums_up_to(4000000).select{ |n| n.even? }.sum
    end
  end
end
