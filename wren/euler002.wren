class FibonacciIterator is Sequence {
    construct new() {
        _a = 0
        _b = 1
    }

    iterate(value) {
        if (value == null) {
            _a = 0
            _b = 1
            return 1
        } else {
            var temp = _b
            _b = _a + _b
            _a = temp
            return _b
        }
    }

    iteratorValue(value) {
        return value
    }

    until(value) {
        var list = []
        for (i in this) {
            if (i > value) {
                break
            } else {
                list.add(i)
            }
        }
        return list
    }
}

class Euler002 {
    static solution {
        var myFib = FibonacciIterator.new()

        return myFib.until(4000000).where { |n| n % 2 == 0 }.reduce { |x, y| x + y }
    }
}

System.print(Euler002.solution)