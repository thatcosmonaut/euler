require 'prime'

def pandigital?(n)
  n.to_s.split('').map(&:to_i).sort == (1..n.to_s.length).to_a
end

def largest_pandigital_prime_up_to(n)
  Prime.take_while { |i| i < n }.select { |i| pandigital?(i) }.max
end
