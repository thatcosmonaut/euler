module Euler
  module Problem001
    extend self

    def solution
      (1..999).select { |n| (n % 3 == 0) || (n % 5 == 0) }.sum
    end
  end
end
