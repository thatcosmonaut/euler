require "./prime"

module Euler
  module Problem010
    extend self

    def solution
      prime = Euler::Prime.new
      prime.take_while { |x| x < 2000000 }.sum
    end
  end
end
