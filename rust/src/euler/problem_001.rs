pub fn solution() -> i32 {
    (1..1000).filter( |n| (n % 3 == 0) || (n % 5 == 0)).sum()
}

#[cfg(test)]
mod tests {
    use euler::problem_001::solution;

    #[test]
    fn problem_001() {
        assert_eq!(solution(), 233_168);
    }
}
