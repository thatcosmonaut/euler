use euler::prime;

pub fn solution() -> Option<u64> {
    prime::trial_factorization(600_851_475_143).last().cloned()
}

#[cfg(test)]
mod tests {
    use euler::problem_003::solution;

    #[test]
    fn problem_003() {
        assert_eq!(solution().unwrap(), 6857);
    }
}