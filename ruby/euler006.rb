def sum_squared(n)
  (1..n).inject(:+) ** 2
end

def square_sums(n)
  (1..n).map { |i| i ** 2 }.inject(:+)
end

def solution
  sum_squared(100) - square_sums(100)
end
