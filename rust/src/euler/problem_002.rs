struct Fibonacci {
    curr: u32,
    next: u32,
}

impl Iterator for Fibonacci {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        let new = self.curr + self.next;

        self.curr = self.next;
        self.next = new;

        Some(self.curr)
    }
}

fn fibonacci() -> Fibonacci {
    Fibonacci { curr: 1, next: 1 }
}

fn fibonacci_sum_up_to(number: u32) -> u32 {
    fibonacci().take_while(|n| *n < number).filter(|n| n % 2 == 0).sum()
}

pub fn solution() -> u32 {
    fibonacci_sum_up_to(4_000_000)
}

#[cfg(test)]
mod tests {
    use euler::problem_002::solution;

    #[test]
    fn problem_002() {
        assert_eq!(solution(), 4_613_732);
    }
}