def pandigital?(n)
  n.to_s.split('').map(&:to_i).sort == [1,2,3,4,5,6,7,8,9]
end

def largest_pandigital_composition_up_to(n)
  largest_pandigital = 1

  (2..n).each do |i|
    digit_list = []
    multiplier = 1
    until digit_list.length >= 9
      digit_list += (i * multiplier).to_s.split('').map(&:to_i)
      multiplier += 1
    end
    number = digit_list.join('').to_i
    if pandigital?(number)
      largest_pandigital = number if number > largest_pandigital
    end
  end

  largest_pandigital
end
