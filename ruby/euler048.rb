def series(n)
  (1..n).map { |x| x ** x }.inject(:+)
end
