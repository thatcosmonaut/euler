import "./euler" for Prime

class Euler003 {
    static solution {
        return Prime.prime_factors(600851475143)[-1]
    }
}

System.print(Euler003.solution)