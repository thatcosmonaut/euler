def divisors(n)
 divisors = [1]
  (2..Math.sqrt(n)).each do |possible_divisor|
    if n % possible_divisor == 0
      divisors << possible_divisor
      divisors << (n / possible_divisor)
    end
  end
  divisors.uniq
end

def abundant?(num)
  divisors(num).inject(:+) > num
end

def abundants
  [].tap do |abundants|
    (1..28122).each do |num|
      abundants << num if abundant?(num)
    end
  end
end

def sums_of_abundants
  [].tap do |sums|
    abundant_list = abundants
    abundant_list.each_with_index do |num, index|
      abundant_list.drop(index).each do |second_num|
        sum = num + second_num
        if sum < 28123
          sums << sum
        else
          break
        end
      end
    end
  end
end

def not_sums_of_abundants
  non_abundants = (1..28122).to_a
  sums_of_abundants.uniq.each do |sum|
    non_abundants.delete sum
  end
  non_abundants
end

puts not_sums_of_abundants.inject(:+)
