require 'prime'

def primes_up_to(n)
  Prime.take_while { |x| x < n }
end

def integer_rotations(n)
  rotations = []
  digits = n.to_s.split('')

  digits.count.times do
    rotations << digits.join('').to_i
    digits.rotate!
  end

  rotations
end

def is_circular?(n)
  circular = true
  integer_rotations(n).each do |perm|
    return false unless Prime.prime?(perm)
  end
  circular
end

def circular_primes_up_to(n)
  primes_up_to(n).select { |x| puts x; is_circular?(x) }
end
