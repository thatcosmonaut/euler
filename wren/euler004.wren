import "./euler" for Utility

class Euler004 {
    static combination(list, k) {
        if (k == 0) {
            return [[]]
        }

        if (list.isEmpty) {
            return []
        }

        var x = list[0]
        var xs = list.skip(1).toList

        return Utility.extend_list(combination(xs, k - 1).map{ |t|
            return Utility.extend_list([x], t)
        }.toList, combination(xs, k))
    }

    static products_of_three_digits {
        return combination((100..999).toList, 2).map { |pair| pair.reduce { |x, y| x * y }}
    }

    static solution {
        return Utility.max(products_of_three_digits.where { |x| Utility.palindrome(x) })
    }
}

System.print(Euler004.solution)