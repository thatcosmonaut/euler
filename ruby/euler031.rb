def change(n, list_of_coins)
  change_helper(n, list_of_coins, [])
end

def change_helper(n, list_of_coins, result_list)
  if n == 0
    return [result_list]
  elsif list_of_coins.empty?
    return []
  else
    list_of_coins.each do |coin|
      if n < coin
        return []
      else
        return change_helper(n - coin, list_of_coins, result_list + [coin]) + change_helper(n, list_of_coins - [coin], result_list)
      end
    end
  end

end
