require 'prime'

def solution
  diagonal_primes = []
  diagonal_composites = []

  increment = 2
  num = 1
  loop do
    4.times do
      num += increment
      if num.prime?
        diagonal_primes << num
      else
        diagonal_composites << num
      end
      return num if (diagonal_primes.count.to_f / diagonal_composites.count.to_f) < 0.1
    end
  end
end
