#include "euler.h"
#include "int_list.h"
#include <stdio.h>

int_list *fibonacci_until(int num)
{

  static int_list list;
  int_list_init(&list);

  int term_one = 1;
  int term_two = 2;
  while (term_one < num)
  {
    if (term_one % 2 == 0) { int_list_add(&list, term_one); }
    int temp = term_two;
    term_two = term_two + term_one;
    term_one = temp;
  }
  return &list;
}

int main()
{
  int_list *result;
  result = fibonacci_until(4000000);
  print_result(int_list_sum(result));
  return 0;
}
