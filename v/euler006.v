import euler

fn main() {
	range := euler.range(1, 100)
	sum := range.reduce(euler.add, 0)
	square_of_sum := sum * sum
	sum_of_squares := range.map(it * it).reduce(euler.add, 0)
	println(square_of_sum - sum_of_squares)
}
