(load "euler.lisp")

(defun is-evenly-divisible-by-all-up-to (n int)
  (eq 0 (sum (mapcar (partial #'mod n) (range 2 int))))
)

(defun solution ()
  (loop for i from 2 by 2 when (is-evenly-divisible-by-all-up-to i 20) do (return i))
)
