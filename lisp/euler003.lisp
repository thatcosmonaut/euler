(load "euler.lisp")

(defun prime-factors (n)
  (select (lambda (a) (eq (mod n a) 0)) (eratosthenes (sqrt n)))
)

(defun solution () (first (reverse (prime-factors 600851475143))))
