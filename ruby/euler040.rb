def champernowe_constant_up_to(n)
  (1..n).to_a.join('')
end

def solution
  champernowe = champernowe_constant_up_to(1000000)
  digits = []
  digits << champernowe[0]
  digits << champernowe[9]
  digits << champernowe[99]
  digits << champernowe[999]
  digits << champernowe[9999]
  digits << champernowe[99999]
  digits << champernowe[999999]
  digits.map(&:to_i).inject(:*)
end
