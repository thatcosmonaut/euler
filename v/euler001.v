import euler

fn multiples_of_three_and_five_below(n int) []int {
  nums := euler.range(3, n-1)
  return nums.filter(it % 3 == 0 || it % 5 == 0)
}

fn main() {
	println(multiples_of_three_and_five_below(1000).reduce(euler.add, 0))
}
