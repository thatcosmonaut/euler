require "spec"
require "../euler009"

describe Euler::Problem009 do
  it "should return 31875000" do
    Euler::Problem009.solution.should eq 31875000
  end
end
