def factorial(n)
  i = 1
  while (n > 0)
    i *= n
    n -= 1
  end
  i
end

def curious_numbers(range)
  solutions = []
  (3..range).each do |i|
    digits = i.to_s.split('').map(&:to_i)
    solutions << i if digits.map { |x| factorial(x) }.inject(:+) == i
  end
  solutions
end
