module Main where

up_to x = take (x-1) (iterate (1+) 1)

divisible_by_3_or_5 x = (x `mod` 3 == 0) || (x `mod` 5 == 0)

main = do
    print (sum (filter divisible_by_3_or_5 (up_to 1000)))
