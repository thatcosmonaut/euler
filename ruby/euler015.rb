require_relative 'euler'

def lattice_paths(size)
  (2*size).choose(size)
end

def solution
  lattice_paths(20)
end
