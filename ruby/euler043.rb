def pandigital?(n)
  n.to_s.split('').map(&:to_i).sort == (1..n.to_s.length).to_a
end

def pandigitals
  [0,1,2,3,4,5,6,7,8,9].permutation.to_a
end

def digit_list_to_int(digits)
  digits.join('').to_i
end

def tridigit_additive_property?(digits)
  return false unless digit_list_to_int([digits[1],digits[2],digits[3]]) % 2 == 0
  return false unless digit_list_to_int([digits[2],digits[3],digits[4]]) % 3 == 0
  return false unless digit_list_to_int([digits[3],digits[4],digits[5]]) % 5 == 0
  return false unless digit_list_to_int([digits[4],digits[5],digits[6]]) % 7 == 0
  return false unless digit_list_to_int([digits[5],digits[6],digits[7]]) % 11 == 0
  return false unless digit_list_to_int([digits[6],digits[7],digits[8]]) % 13 == 0
  return false unless digit_list_to_int([digits[7],digits[8],digits[9]]) % 17 == 0
  true
end

def pandigitals_with_additive_property
  pandigitals.select { |n| tridigit_additive_property?(n) }.map { |n| n.join('').to_i }
end
