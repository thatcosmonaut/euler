(load "euler.lisp")

(defun is_multiple_of_3_or_5 (n) (or (eq 0 (mod n 3)) (eq 0 (mod n 5))))
(defun multiples_of_3_or_5 (n) (select #'is_multiple_of_3_or_5 (range 1 n)))
(defun result (n) (sum (multiples_of_3_or_5 n)))
