require "spec"
require "../euler010"

describe Euler::Problem010 do
  it "should return 142913828922" do
    Euler::Problem010.solution.should eq 142913828922
  end
end
