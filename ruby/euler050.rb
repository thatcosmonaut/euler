require 'prime'

def longest_sum_of_consecutive_primes_beneath(n)
  sum = 0
  primes = Prime.take_while { |x| (sum += x) < n }
  longest_prime_count = 0
  longest_prime_sum = 0
  prime_list = []
  (1..primes.count).each do |x|
    primes.each_cons(x) do |y|
      subcount = y.inject(:+)
      if Prime.prime?(subcount)
        if y.count > longest_prime_count
          longest_prime_sum = subcount
          longest_prime_count = y.count
          prime_list = y
        end
      end
    end
  end
  [prime_list, longest_prime_sum]
end
