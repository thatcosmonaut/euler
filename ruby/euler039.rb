def is_right_triangle?(a, b, c)
  a ** 2 + b ** 2 == c ** 2
end

def generate_triangle_sides(perimeter)
  triangles = []
  (1..perimeter).each do |i|
    (i..perimeter-i).each do |j|
      triangles << [i, j, perimeter-i-j]
    end
  end
  triangles
end

def integer_right_triangle_solutions(perimeter)
  triangles = generate_triangle_sides(perimeter)
  triangles.select { |x| is_right_triangle?(x[0], x[1], x[2]) }
end

def p_with_max_solution_count_up_to(n)
  max_solution_count = 0
  p = 2
  (3..n).each do |i|
    solution_count = integer_right_triangle_solutions(i).count
    if solution_count > max_solution_count
      max_solution_count = solution_count
      p = i
    end
  end
  p
end
