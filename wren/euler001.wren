class Euler001 {
    static multiples_of_three_and_five_below(n) {
        return (3..n-1).where { |i| (i % 3 == 0) || (i % 5 == 0) }
    }

    static solution {
        return multiples_of_three_and_five_below(1000).reduce { |x,y| x + y }
    }
}

System.print(Euler001.solution)