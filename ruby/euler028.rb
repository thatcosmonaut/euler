def spiral(size)
  max_value = size**2

  diagonals = [1]
  diagonal_increase_value = 2
  current_num = 1

  until current_num == max_value
    4.times do
      current_num += diagonal_increase_value
      diagonals << current_num
    end
    diagonal_increase_value += 2
  end

  diagonals.inject(:+)
end
