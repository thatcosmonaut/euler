require 'prime'

def truncate_right(n)
  n.to_s[0..-2].to_i
end

def truncate_left(n)
  n.to_s[1..-1].to_i
end

def prime_truncatable_right?(n)
  return false unless Prime.prime?(n)
  truncated_prime = n
  until (truncated_prime = truncate_right(truncated_prime)) == 0
    return false unless Prime.prime?(truncated_prime)
  end
  return true
end

def prime_truncatable_left?(n)
  return false unless Prime.prime?(n)
  truncated_prime = n
  until (truncated_prime = truncate_left(truncated_prime)) == 0
    return false unless Prime.prime?(truncated_prime)
  end
  return true
end


def primes_truncatable_up_to(n)
  (1..n).select { |i| prime_truncatable_right?(i) && prime_truncatable_left?(i) }
end
