require_relative 'euler'

def solution
  600851475143.prime_factors.max
end
