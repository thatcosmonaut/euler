import euler
import math

fn factorization_of_integer_divisible_by_all_integers_up_to(n int) map[string]int {
	mut result := map[string]int
	for i in euler.range(2, n) {
		for p, e in euler.prime_factorization(i) {
			if !(p in result) || e > result[p] {
				result[p] = e
			}
		}
	}
	return result
}

fn factors_to_int(factorization map[string]int) int {
	mut result := 1
	for p, e in factorization {
		result *= int(math.pow(p.int(), e))
	}
	return result
}

fn main() {
	println(factors_to_int(factorization_of_integer_divisible_by_all_integers_up_to(20)))
}
