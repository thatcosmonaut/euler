(load "euler.lisp")

(defun solution ()
  (first (reverse (loop for i from 2 when (prime i) collect i into prime-list when (eq 10001 (length prime-list)) return prime-list)))
)
