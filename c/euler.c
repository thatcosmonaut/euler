#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct unsigned_int_sized_array {
  int size;
  int allocated_size;
  unsigned int *elements_ptr;
} unsigned_int_sized_array;

/* typedef struct unsigned_int_sized_array thing; */

unsigned_int_sized_array unsigned_int_sized_array_init() {
  unsigned_int_sized_array array;
  array.size = 0;
  array.allocated_size = 16;
  unsigned int * elts = malloc(sizeof(unsigned int) * 16);
  array.elements_ptr = elts;
  return array;
}

void reallocate(unsigned_int_sized_array array) {
  array.allocated_size *= 2;
  realloc(array.elements_ptr, sizeof(unsigned int) * array.allocated_size);
}

void push(unsigned_int_sized_array array, unsigned int new_elt) {
  if (array.size >= array.allocated_size) {
    reallocate(array);
  }
  array.elements_ptr[array.size] = new_elt;
  array.size++;
}

void print_result(int result) {
  printf("%d\n", result);
}
