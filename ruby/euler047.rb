require 'prime'

def solution_up_to(n)
  (1..n).map { |x| Prime.prime_division(x).count }.each_cons(4).find_index { |x| x.all? { |y| y == 4} } + 1
end
