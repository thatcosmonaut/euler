def even_fibonacci_up_to(n)
  Enumerator.new do |y|
    a = 0
    b = 1
    loop do
      temp = b
      b = a + b
      a = temp
      y << b
    end
  end.take_while { |i| i < n }.select { |i| i.even? }
end

def solution
  even_fibonacci_up_to(4000000).inject(:+)
end
