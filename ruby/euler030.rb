def sum_of_fifth_powers(num)
  list_of_digits = num.to_s.split("").map(&:to_i)
  list_of_digits.map { |digit| digit**5 }.inject(:+)
end

equivalent_sums = []
(10..1000000).each do |num|
  sum = sum_of_fifth_powers(num)
  equivalent_sums << num if sum == num
end

puts equivalent_sums.inject(:+)
