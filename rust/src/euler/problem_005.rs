use euler::math;

fn smallest_divisible_by_all_up_to(n: u64) -> u64 {
    (2..n).fold(1, |acc, i| {
        acc * i / math::gcd(i, acc)
    })
}

pub fn solution() -> u64 {
    smallest_divisible_by_all_up_to(20)
}

#[cfg(test)]
mod tests {
    use euler::problem_005::solution;

    #[test]
    fn problem_005() {
        assert_eq!(solution(), 232_792_560);
    }
}