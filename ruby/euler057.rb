require_relative 'euler'

def root_two_expansion(iterations)
  1 + Rational(1,decimal_expansion(iterations))
end

def decimal_expansion(iterations)
  if iterations == 0
    return 2
  else
    return 2 + Rational(1,decimal_expansion(iterations - 1))
  end
end

def solution
  (0..999).map { |n| root_two_expansion(n) }.count { |r| r.numerator.to_digit_list.count > r.denominator.to_digit_list.count }
end
