#include "euler.h"
#include "generic_list.h"
#include <math.h>
#include <stdbool.h>

bool prime(long int n) {
  for (long i = 2; i <= sqrt(n); i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

generic_list *factors(long n) {
  static generic_list list;
  generic_list_init(&list);
  for (long i = 2; i <= sqrt(n); i++) {
    if (n % i == 0) {
      if (prime(i)) {
        generic_list_add(&list, LONG, &i);
      }
    }
  }
  return &list;
}

int main() {
  generic_list *result;

  result = factors(600851475143);
  generic_list_print(result);

  return 0;
}
