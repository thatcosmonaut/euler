require "./euler"

module Euler
  module Problem003
    extend self

    def solution
      Euler::Prime.trial_division(600851475143).max
    end
  end
end
