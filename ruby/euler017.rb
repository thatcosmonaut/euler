require 'numbers_and_words'

class Integer
  def words_length
    if self < 100 || self % 100 == 0
      self.to_words.gsub(/\s+/, "").gsub(/\-/, "").length
    else
      self.to_words.gsub(/\s+/, "").gsub(/\-/, "").length + 3 #accounting for "and"
    end
  end
end

count = 0
(1..1000).each do |num|
  count += num.words_length
end

puts count
