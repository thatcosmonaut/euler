function even_fibonacci_up_to(n)
    fibs = []
    first = 1
    second = 1
    push!(fibs, first)
    push!(fibs, second)
    while second < n
        temp = second
        second = first + second
        first = temp
        push!(fibs, second)
    end
    pop!(fibs)
    filter(i -> i % 2 == 0, fibs)
end

function solution()
    reduce(+, even_fibonacci_up_to(4000000))
end

print(solution())
