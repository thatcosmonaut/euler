module Euler
  module Problem006
    extend self

    def solution
      (1..100).sum ** 2 - (1..100).map { |n| n * n }.sum
    end
  end
end
