require_relative 'euler'

def products_of_three_digits
  (100..999).to_a.combination(2).map { |p| p.inject(:*) }
end

def solution
  products_of_three_digits.select { |x| Euler.palindrome?(x) }.max
end
