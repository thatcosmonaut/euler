def collatz(num)
  [].tap do |list|
    until num == 1
      if num.even?
        list << (num = num / 2)
      else
        list << (num = (3 * num) + 1)
      end
    end
    list << 1
  end
end

def solution
  (1..999999).map { |i| collatz(i).count }.each_with_index.max[1] + 1
end
