def permutations?(number_list)
  comparison_digit_list = to_digit_list(number_list.first).sort
  number_list.all? { |x| to_digit_list(x).sort == comparison_digit_list }
end

def six_multiples(x)
  [x, 2 * x, 3 * x, 4 * x, 5 * x, 6 * x]
end

def infinite_series
  Enumerator.new do |y|
    n = 1
    loop do
      y << n
      n += 1
    end
  end
end

def same_digit_six_multiples
  series = infinite_series
  n = series.next
  loop do
    result = permutations?(six_multiples(n))
    return six_multiples(n) if result
    n = series.next
  end
end
