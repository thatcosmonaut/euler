require 'date'

number_of_sundays = 0
(DateTime.new(1901, 1, 1)..DateTime.new(2000, 12, 31)).each do |day|
  if day.strftime("%A") == "Sunday" && day.strftime("%d") == "01"
    number_of_sundays += 1
  end
end
puts number_of_sundays
