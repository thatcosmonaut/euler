function multiples_of_three_and_five_below(n)
    filter(i -> (i % 3 == 0) || (i % 5 == 0), 3:(n-1))
end

function solution()
    reduce(+, multiples_of_three_and_five_below(1000))
end

print(solution())
