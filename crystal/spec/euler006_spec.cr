require "spec"
require "../euler006"

describe Euler::Problem006 do
  it "should return 25164150" do
    Euler::Problem006.solution.should eq 25164150
  end
end
