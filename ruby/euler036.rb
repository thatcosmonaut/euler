def is_palindromic_base_ten?(n)
  n.to_s == n.to_s.reverse
end

def is_palindromic_base_two?(n)
  n.to_s(2) == n.to_s(2).reverse
end

def palindromic_up_to(n)
  (1..n).select { |i| is_palindromic_base_two?(i) && is_palindromic_base_ten?(i) }
end
