#ifndef INT_LIST_H_
#define INT_LIST_H_

typedef struct int_list_ {
  int* data;
  int size;
  int count;
} int_list;

void int_list_init(int_list *l);
int int_list_count(int_list *l);
void int_list_add(int_list *l, int value);
int int_list_get(int_list *l, int index);
void int_list_set(int_list*l, int index, int value);
void int_list_delete(int_list *l, int index);
int int_list_sum(int_list *l);
void int_list_print(int_list *l);
void int_list_free(int_list *l);

#endif
