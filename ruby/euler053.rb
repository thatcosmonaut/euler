require_relative 'euler'

def solution
  result = 0
  (1..100).each do |n|
    (1..n).each do |k|
      result += 1 if n.choose(k) > 1000000
    end
  end
  result
end
