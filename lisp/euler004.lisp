(load "euler.lisp")

(defun three-digit-products ()
  (resolve-tuples (lambda (a b) (* a b)) (cartesian (range 100 999) (range 100 999)))
)

(defun solution ()
  (max-of (select #'is-palindrome (three-digit-products)))
)
