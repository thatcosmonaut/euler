require "spec"
require "../euler008"

describe Euler::Problem008 do
  it "should return 23514624000" do
    Euler::Problem008.solution.should eq 23514624000
  end
end
