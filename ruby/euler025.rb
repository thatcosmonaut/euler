#starts with index 1
def fib(n)
  if n == 1|| n == 2
    1
  else
    fib(n-1) + fib(n-2)
  end
end

fib = Enumerator.new do |y|
  n_1 = n_2 = 1
  loop do
    y << n_1
    n_1, n_2 = n_2, n_1 + n_2
  end
end

term = 0
while fib.next.to_s.length < 1000
  term += 1
end

puts term + 1

# next_fib = fib.next
# i = 0
# while next_fib.to_s.length < 1000
#   next_fib = fib.next
#   i += 1
# end
# 
# puts i+1
