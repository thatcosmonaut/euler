require_relative 'euler'

def solution
  [].tap do |sums|
    (1..100).each do |a|
      (1..100).each do |b|
        sums << (a ** b).to_digit_list.inject(:+)
      end
    end
  end.max
end
