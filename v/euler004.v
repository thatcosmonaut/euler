import euler

fn main()
{
	mut max := 0
	for combo in euler.two_combinations(euler.range(100, 999)) {
		product := combo[0] * combo[1]
		if product > max && euler.palindrome(product) {
			max = product
		}
	}
	println(max)
}
