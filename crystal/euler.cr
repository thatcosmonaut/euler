# miscellaneous stuff

require "big"

module Euler
  extend self

  alias NumType = Int32 | Int64 | UInt32 | UInt64

  def to_big_ints(num_list : Array(NumType))
    num_list.map { |n| BigInt.new(n) }
  end

  def to_digit_list(n : NumType | BigInt)
    n.to_s.chars.map { |d| d.to_i }
  end

  def palindrome?(x)
    x.to_s.reverse == x.to_s
  end
end
