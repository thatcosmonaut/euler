require "spec"
require "../euler003"

describe Euler::Problem003 do
  it "should return 6857" do
    Euler::Problem003.solution.should eq 6857
  end
end
