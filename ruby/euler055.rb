require_relative 'euler'

def reverse_and_add(n)
  n + Euler.from_digit_list(n.to_digit_list.reverse)
end

def lychrel_number?(n)
  50.times do
    n = reverse_and_add(n)
    return false if Euler.palindrome?(n)
  end
  true
end

def solution
  (1..10000).count { |x| lychrel_number?(x) }
end
