require_relative 'euler'

def triangle_enumerator
  Enumerator.new do |y|
    sum = 1
    next_num = 2
    loop do
      y << sum
      sum += next_num
      next_num += 1
    end
  end
end

def solution
  triangle_enumerator.take_while { |x| x.divisors.count <= 500 }.last
end
