require "spec"
require "../euler007"

describe Euler::Problem007 do
  it "should return 104743" do
    Euler::Problem007.solution.should eq 104743
  end
end
