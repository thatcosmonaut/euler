require "./euler"

module Euler
  module Problem004
    extend self

    def products_of_three_digit_nums
      (100..999).to_a.combinations(2).map { |p| p.product }
    end

    def solution
      products_of_three_digit_nums.select { |x| Euler.palindrome?(x) }.max
    end
  end
end
